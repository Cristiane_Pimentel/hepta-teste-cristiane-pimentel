var campoFiltro = document.querySelector("#filtrar-tabela");
var botaoBuscar = document.querySelector("#botao-buscar");

botaoBuscar.addEventListener("click", function() {
	var registros = document.querySelectorAll(".registro");

	if (campoFiltro.value.length > 0) {
		for (var i = 0; i < registros.length; i++) {
			var registro = registros[i];

			var tdNome = registro.querySelector(".info-nome");
			var nome = tdNome.textContent;

			var tdId = registro.querySelector(".info-id");
			var id = tdId.textContent;

			var expressao = new RegExp(campoFiltro.value, "i");

			var texto = tdNome.textContent + tdId.textContent;


			if (expressao.test(texto)){
				registro.classList.remove("invisivel");
			} else {
				registro.classList.add("invisivel");
			}

		}
	} else {
		for (var i = 0; i < registros.length; i++) {
			var registro = registros[i];
			registro.classList.remove("invisivel");
		}
	}
});

var botaoLimpar = document.querySelector("#clean");

botaoLimpar.addEventListener("click", function() {
	var registros = document.querySelectorAll(".registro");
	for (var i = 0; i < registros.length; i++) {
		var registro = registros[i];
		registro.classList.remove("invisivel");
	}
});

var botaoSalvar = document.querySelector("#salvar");
var campoNome = document.querySelector("#campoDeNome"); 
var spanSucesso = document.querySelector("#mensagem-sucesso");

botaoSalvar.addEventListener("click", function() {
	console.log(campoNome.value);
	if (campoNome.value.length > 0){
		spanSucesso.textContent = "Nome adicionado com sucesso";
	}
	setTimeout(function() {
		spanSucesso.classList.add("invisivel");
	}, 1000);
});

